package com.example.producer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.producer.dto.EmailTemplate;
import com.example.producer.kafkasender.MessageService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class MessageController {
	private final MessageService messageService;

	@PostMapping("/emails")
	public ResponseEntity<String> sendMail(@RequestBody EmailTemplate emailTemplate) {
		return ResponseEntity.status(HttpStatus.OK).body(messageService.sendMail(emailTemplate));
	}
}
