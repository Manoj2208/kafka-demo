package com.example.producer.kafkasender;

import org.springframework.stereotype.Service;

import com.example.producer.config.ProducerService;
import com.example.producer.dto.EmailTemplate;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MessageService {
	private final ProducerService producerService;

	public String sendMail(EmailTemplate emailTemplate) {
		producerService.send(emailTemplate);
		return "Email template published to kafka";
	}

}
