package com.example.producer.config;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import com.example.producer.dto.EmailTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProducerService {
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(EmailTemplate emailTemplate) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, EmailTemplate> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());

		log.info("Message Sent: " + emailTemplate);
		ProducerRecord<String, EmailTemplate> auditRecord = new ProducerRecord<>("email-queue", emailTemplate);
		log.info("" + auditRecord);
		log.info("" + kafkaProducer.send(auditRecord));
	}
}
