package com.example.producer.dto;

import lombok.Builder;

@Builder
public record EmailTemplate(String from,String to,String subject,String body) {

}
