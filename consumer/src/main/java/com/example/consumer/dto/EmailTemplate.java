package com.example.consumer.dto;

import lombok.Builder;

@Builder
public record EmailTemplate(String from,String to,String subject,String body) {

}
