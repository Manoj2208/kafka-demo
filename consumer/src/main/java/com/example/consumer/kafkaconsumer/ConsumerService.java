package com.example.consumer.kafkaconsumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class ConsumerService {

	@KafkaListener(topics = "email-queue", groupId = "consumer-0")
	public void consumeAuditLog(ConsumerRecord<String, Object> emailTemplate) {
		log.info("EmailTemplate consumed from consumer" + emailTemplate.value());

	}
}
